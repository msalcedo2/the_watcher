"""the_watcher

The DomainTools the_watcher Command Line Tool"

For coverage see:
[Coverage Report](https://backend.pages.domaintools.net/tools/the_watcher/coverage/).
"""
__version__ = "0.0.1"
__python_requires__ = ">=3.11.0"
