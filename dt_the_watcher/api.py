import asyncio
import logging
import uvicorn
import schedule

from contextlib import asynccontextmanager
from typing import Optional
from datetime import datetime
from fastapi import FastAPI, HTTPException, Request, status
from fastapi.responses import JSONResponse

from dt_the_watcher.models import ApiProductNotAvailableException, InvalidDateRangeException
from .sqlitedb import close_conn, create_table, API_PRODUCT_CHECKS_TABLE
from .report_processor import run_checks, run, generate_stats


@asynccontextmanager
async def lifespan(app: FastAPI):
    global background_task
    log.info("Starting startup")

    create_table(API_PRODUCT_CHECKS_TABLE)
    schedule.every(5).seconds.do(run_checks)
    background_task = asyncio.ensure_future(run(dead))

    log.info("Finished startup")
    yield
    log.info("Starting shutdown")

    dead.set()
    close_conn()
    if background_task:
        background_task.cancel()
        await background_task

    log.info("Finished shutdown")

app = FastAPI(lifespan=lifespan)
logging.basicConfig(level=logging.INFO, format='%(asctime)s - %(levelname)s - %(message)s')
log = logging.getLogger(__name__)

dead = asyncio.Event()
background_task: Optional[asyncio.Task] = None


@app.exception_handler(ApiProductNotAvailableException)
async def validation_exception_handler(request: Request, exc: ApiProductNotAvailableException):
    return JSONResponse(
        status_code=status.HTTP_422_UNPROCESSABLE_ENTITY,
        content={"detail": exc.message},
    )


@app.exception_handler(InvalidDateRangeException)
async def validation_exception_handler(request: Request, exc: InvalidDateRangeException):
    return JSONResponse(
        status_code=status.HTTP_400_BAD_REQUEST,
        content={"detail": exc.message},
    )


@app.get("/pulse", tags=["System"])
async def pulse():
    if dead.is_set():
        raise HTTPException(status_code=500, detail="internal server error")
    else:
        return "OK"


@app.get("/api_report/")
async def get_report(start_date: datetime = None, end_date: datetime = None, api_product: str = None):
    current_date = datetime.now()
    if not start_date:
        start_date = datetime(current_date.year, current_date.month, 1)
    if not end_date:
        if current_date.month == 12:
            end_date = datetime(current_date.year + 1, 1, 1)
        else:
            end_date = datetime(current_date.year, current_date.month + 1, 1)

    if start_date > end_date:
        raise InvalidDateRangeException()

    return generate_stats(start_date, end_date, api_product)


def start():
    uvicorn.run(
        "api:app", port=8000, reload=False, loop="asyncio", host="0.0.0.0", log_level="debug"
    )


if __name__ == "__main__":
    start()
