from datetime import datetime, timedelta
from typing import Optional

class ApiProductNotAvailableException(Exception):
    def __init__(self, message = "API product is not available for generating a report"):
        self.message = message
        super().__init__(self.message)


class InvalidDateRangeException(Exception):
    def __init__(self, message="Start date cannot be greater than end date"):
        self.message = message
        super().__init__(self.message)


class Downtime():
    def __init__(self, start: datetime, end: datetime):
        self.start = start
        self.end = end
        self.total = 0

    def reset(self, end_date: datetime = None):
        if self.start is None:
            return

        if self.end is None and end_date:
            self.end = end_date
        elif self.end is None:
            self.end = datetime.now()

        difference: timedelta = self.end - self.start
        self.total += difference.total_seconds()
        self.start = None
        self.end = None
    
    def __str__(self) -> str:
        return f"total: {self.total}"
    
    def __repr__(self) -> str:
        return f"total: {self.total}"


class APIProduct():
    def __init__(self, id: str, url: str, name: str):
        self.id = id
        self.url = url
        self.name = name
        self.downtime = Downtime(None, None)
        self.uptime: Optional[float] = None
        self.no_records = True

    def set_uptime(self, total_time: float):
        uptime = (total_time - self.downtime.total) / total_time
        if self.no_records:
            uptime = None
        else:
            self.no_records = True
        self.uptime = uptime

    def switch_no_records(self):
        if self.no_records:
            self.no_records = False

    def get_uptime_percentage(self) -> float:
        if not self.uptime:
            return 0.0
        return round(self.uptime*100, 2)

    def get_downtime_formatted(self) -> str:
        hours = self.downtime.total // 3600
        remaining_seconds = self.downtime.total % 3600

        minutes = remaining_seconds // 60
        seconds = remaining_seconds % 60
        return f"{hours} hours, {minutes} minutes, and {seconds}"
    
    def __str__(self) -> str:
        return f"Downtime: {self.get_downtime_formatted()} - uptime: {self.get_uptime_percentage()}%"
    
    def __repr__(self) -> str:
        return f"Downtime: {self.get_downtime_formatted()} - uptime: {self.get_uptime_percentage()}%"
