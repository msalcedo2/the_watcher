import asyncio
import requests
import schedule
import time
import logging

from typing import Dict
from datetime import datetime
from .sqlitedb import c, insert_into_values, API_PRODUCT_CHECKS_TABLE
from .models import APIProduct, ApiProductNotAvailableException
from asyncio import Event

products: Dict[str, APIProduct] = {
    "example_api": APIProduct("example_api", "http://example.com", "Example API"), 
    "parsed_whois": APIProduct("parsed_whois", "http://whoisparserapi.lb.domaintools.net/health", "Parsed Whois")
}


def get_api_products() -> Dict[str, APIProduct]:
     return products


def check_api_product(product: APIProduct):
    """ Check the availability of a specified URL by making an HTTP request, and storing the response """
    try:
        start_time = time.time()
        response = requests.get(product.url, timeout=10)
        response_time = time.time() - start_time
        status_code = response.status_code
        insert_into_values(
            API_PRODUCT_CHECKS_TABLE, 
            "(api_id, url, timestamp, status_code, response_time)", 
            "(?, ?, ?, ?, ?)", 
            (product.id, product.url, datetime.now().timestamp(), status_code, response_time)
        )

        if response.status_code == 200:
            logging.debug(f"SUCCESS: {product.url} is up. Time taken: {response_time}s")
        else:
            logging.error(f"ERROR: {product.url} returned {status_code}. Time taken: {response_time}s")
    except requests.RequestException as e:
        logging.error(f"EXCEPTION: {product.url} failed with error {e}")
        insert_into_values(
            API_PRODUCT_CHECKS_TABLE, 
            "(api_id, url, timestamp, status_code, response_time)", 
            "(?, ?, ?, ?, ?)", 
            (product.id, product.url, datetime.now().timestamp(), 0, -1)
        )


def generate_stats(start_date: datetime, end_date: datetime, api_product: str = None, table_name: str = API_PRODUCT_CHECKS_TABLE) -> Dict[str, Dict[str, str]]:
    if api_product and api_product not in get_api_products():
        raise ApiProductNotAvailableException()

    query = f"SELECT * FROM {table_name} WHERE timestamp BETWEEN ? AND ?"
    arguments = (start_date.timestamp(), end_date.timestamp())
    if api_product:
        query += " AND api_id = ?"
        arguments = (start_date.timestamp(), end_date.timestamp(), api_product)
    query += " ORDER BY timestamp"
    c.execute(query, arguments)

    records = c.fetchall()
    total_time = end_date - start_date
    for record in records:
        logging.debug(f"{record}")
        api_id, _, timestamp, status_code, _ = record
        products[api_id].switch_no_records()

        # It had a downtime
        if status_code < 200 or status_code >= 400:
            if not products[api_id].downtime.start:
                products[api_id].downtime.start = datetime.fromtimestamp(timestamp)
        # It recovered or it hadn't downtimes
        else:
            if products[api_id].downtime.start:
                products[api_id].downtime.end = datetime.fromtimestamp(timestamp)
            products[api_id].downtime.reset(end_date)
    
    # Calculate uptime for each api product based on downtimes
    for api_id, product in products.items():
        if api_product and api_id != api_product:
            continue
        product.downtime.reset()
        product.set_uptime(total_time.total_seconds())

    api_product_stats = dict()
    for api_id, product in products.items():
        if api_product and api_id != api_product:
            continue

        if not product.uptime:
            api_product_stats[api_id] = {"Uptime": f"NaN", "Downtime": f"NaN", "detail": f"There aren't records between {start_date} and {end_date}"}
        else:
            api_product_stats[api_id] = {"Uptime": f"{product.get_uptime_percentage()}%", "Downtime": f"{product.get_downtime_formatted()}"}
    
    return api_product_stats


def run_checks():
    for product in products.values():
        check_api_product(product)


async def run(dead: Event):
    try:
        while not dead.is_set():
            schedule.run_pending()
            await asyncio.sleep(5)
    except asyncio.CancelledError:
        logging.info("Checks Cancelled")
