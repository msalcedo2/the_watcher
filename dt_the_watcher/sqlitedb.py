import sqlite3
import logging

API_PRODUCT_CHECKS_TABLE = "api_product_checks"
conn = sqlite3.connect('watcher.db')
c = conn.cursor()

def create_table(table_name: str):
    try:
        c.execute(f'''
            CREATE TABLE IF NOT EXISTS {table_name} (
                api_id TEXT,
                url TEXT,
                timestamp DATE,
                status_code INTEGER,
                response_time REAL
            )
        ''')
        conn.commit()
    except Exception as e:
        logging.error(f"ERROR: {e}")


def insert_into_values(table_name: str, columns: str, values: str, arguments: tuple):
    c.execute(f'INSERT INTO {table_name} {columns} VALUES {values}', arguments)
    conn.commit()


def backup_table(source_table_name: str, backup_table_name: str):
    drop_Table(backup_table_name)

    c.execute(f"CREATE TABLE {backup_table_name} AS SELECT * FROM {source_table_name}")
    conn.commit()


def drop_Table(table_name: str):
    c.execute(f"DROP TABLE IF EXISTS {table_name}")
    conn.commit()


def close_conn():
    conn.close()
