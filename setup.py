"""Python installation definition for the_watcher"""
import os
import re

from setuptools import setup

PACKAGE = "dt_the_watcher"
PACKAGE_PATH = os.path.join(os.path.dirname(__file__), PACKAGE)

with open(os.path.join(PACKAGE_PATH, "__init__.py")) as package_file:
    package_contents = package_file.read()
    version_match = re.search(r"""^__version__\s*=\s*['"]([^'"]*)['"]""", package_contents, re.M)
    if version_match:
        VERSION = version_match.group(1)
    else:
        raise RuntimeError("No __version__ specified in {}".format(package_file.name))

    python_requirement_match = re.search(
        r"""^__python_requires__\s*=\s*['"]([^'"]*)['"]""", package_contents, re.M
    )
    if python_requirement_match:
        PYTHON_REQUIREMENT = python_requirement_match.group(1)
    else:
        raise RuntimeError("No __python_requires__ specified in {}".format(package_file.name))

setup(
    name=PACKAGE,
    version=VERSION,
    python_requires=PYTHON_REQUIREMENT,
    description="The DomainTools the_watcher Command Line Tool",
    author="DomainTools",
    author_email="support@domaintools.com",
    url="http://www.domaintools.com/",
    install_requires=["dt_tool_requirements"],
    entry_points={"console_scripts": ["{0} = {0}.cli:__hug__.cli".format(PACKAGE)]},
    packages=[PACKAGE],
)
