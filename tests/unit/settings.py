"""Defines all test wide settings and variables"""
from vcr import VCR

vcr = VCR(
    cassette_library_dir="tests/fixtures/vcr/",
    path_transformer=VCR.ensure_suffix(".yaml"),
    record_mode="new_episodes",
)
