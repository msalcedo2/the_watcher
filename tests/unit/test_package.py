"""Tests to ensure the DomainTool's the_watcher package is working as intended"""
import dt_the_watcher as tool_package


def test_import():
    """Basic test case to ensure current  tool package is importable"""
    assert tool_package.__version__
