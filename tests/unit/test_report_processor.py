"""Tests to ensure the the_watcher is working as intended"""
import pytest

from datetime import datetime
from typing import Dict, List, Optional
from dt_the_watcher.models import ApiProductNotAvailableException
from dt_the_watcher.report_processor import APIProduct, generate_stats, get_api_products
from dt_the_watcher.sqlitedb import API_PRODUCT_CHECKS_TABLE, insert_into_values, drop_Table, create_table

TEST_CHECKS_TABLE = "test_api_product_checks"

products_test: Dict[str, APIProduct] = {
    "example_api_1": APIProduct("example_api_1", "url", "Example API 1"),
    "example_api_2": APIProduct("example_api_2", "url", "Example API 2"),
    "example_api_3": APIProduct("example_api_3", "url", "Example API 3"),
    "example_api_4": APIProduct("example_api_4", "url", "Example API 4"),
    "example_api_5": APIProduct("example_api_5", "url", "Example API 5"),
    "example_api_6": APIProduct("example_api_6", "url", "Example API 6"),
}

@pytest.fixture
def populate_data():
    drop_Table(TEST_CHECKS_TABLE)
    create_table(TEST_CHECKS_TABLE)

    test_data = [
        ("example_api_1", "url", datetime(2024, 5, 3, 6), 200, 0),
        ("example_api_1", "url", datetime(2024, 5, 3, 7), 200, 0),
        ("example_api_1", "url", datetime(2024, 5, 3, 8), 404, 0),
        ("example_api_1", "url", datetime(2024, 5, 3, 9), 404, 0),
        ("example_api_1", "url", datetime(2024, 5, 3, 10), 200, 0),
        ("example_api_2", "url", datetime(2024, 5, 3, 6), 200, 0),
        ("example_api_2", "url", datetime(2024, 5, 3, 7), 404, 0),
        ("example_api_2", "url", datetime(2024, 5, 3, 8), 200, 0),
        ("example_api_2", "url", datetime(2024, 5, 3, 9), 404, 0),
        ("example_api_2", "url", datetime(2024, 5, 3, 10), 200, 0),
        ("example_api_3", "url", datetime(2024, 5, 3, 6), 200, 0),
        ("example_api_3", "url", datetime(2024, 5, 3, 7), 404, 0),
        ("example_api_4", "url", datetime(2024, 5, 3, 6), 404, 0),
        ("example_api_4", "url", datetime(2024, 5, 3, 7), 200, 0),
        ("example_api_5", "url", datetime(2024, 5, 3, 6), 404, 0),
        ("example_api_6", "url", datetime(2024, 5, 3, 6), 200, 0),
    ]

    for api_id, url, timestamp, status_code, response_time in test_data:
        insert_into_values(
            TEST_CHECKS_TABLE, 
            "(api_id, url, timestamp, status_code, response_time)", 
            "(?, ?, ?, ?, ?)",
            (api_id, url, timestamp.timestamp(), status_code, response_time)
        )

def test_generate_stats(mocker, populate_data):
    mocker.patch('dt_the_watcher.report_processor.products', new=products_test)

    start_date = datetime(2024, 5, 3)
    end_date = datetime(2024, 5, 4, 0)
    _ = generate_stats(start_date, end_date, None, TEST_CHECKS_TABLE)
    api_products = get_api_products()

    assert all([True if product.uptime else False for product in api_products.values()])


def test_generate_stats_percentages(mocker, populate_data):
    # Requesting stats for one day
    # 2 hours unavailable = 8,33 %
    # 91,67 % Uptime = 79200 seconds
    mocker.patch('dt_the_watcher.report_processor.products', new={"example_api_1": APIProduct("example_api_1", "url", "Example API 1")})

    start_date = datetime(2024, 5, 3)
    end_date = datetime(2024, 5, 4, 0)
    delta = end_date - start_date
    _ = generate_stats(start_date, end_date, "example_api_1", TEST_CHECKS_TABLE)
    api_products = get_api_products()

    assert all([True if product.uptime else False for product in api_products.values()])
    assert api_products["example_api_1"].uptime * delta.total_seconds() == 79200
    assert api_products["example_api_1"].downtime.total == 2*60*60 # 2 hours in seconds


def test_generate_stats_for_no_available_api_product(mocker):
    mocker.patch('dt_the_watcher.report_processor.products', new={"example_api_1": APIProduct("example_api_1", "url", "Example API 1")})

    start_date = datetime(2024, 5, 3)
    end_date = datetime(2024, 5, 4, 0)
    try:
        _ = generate_stats(start_date, end_date, "not_real_api_product", TEST_CHECKS_TABLE)
        assert False
    except ApiProductNotAvailableException:
        assert True
